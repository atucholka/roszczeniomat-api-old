using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using roszczeniomat.Models;
using roszczeniomat.Interfaces;
using roszczeniomat.Entities;

namespace roszczeniomat.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("EnableCORS")]
    public class ItemShareController : Controller
    {
        private readonly MainContext _context;
        private readonly IItemShareService _itemShareService;

        public ItemShareController(MainContext context, IItemShareService itemShareService)
        {
            _context = context;
            _itemShareService = itemShareService;
        }

        [HttpPost]
        public async Task<IActionResult> AddItemShareAsync([FromBody]ItemShareBasicEntity itemShare)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _itemShareService.AddItemShareAsync(itemShare);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _itemShareService.DeleteItemShareAsync(id);
            return Ok();
        }
    }
}
