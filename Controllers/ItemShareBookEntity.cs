namespace roszczeniomat.Entities
{
    public class ItemShareBookEntity: BaseEntity
    {
        public int ItemId { get; set; }
        public int BenefactorId { get; set; }
        public bool Book { get; set; }
        
    }
}