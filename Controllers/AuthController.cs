using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore;
using roszczeniomat.Models;

namespace roszczeniomat.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("EnableCORS")]
    public class AuthController : Controller
    {
        private readonly MainContext _context;

        public AuthController(MainContext context)
        {
            _context = context;
        }

        // GET api/values
        [HttpPost]
        public async Task<IActionResult> Login([FromBody]Users user)
        {
            if (user == null)
                return BadRequest("User jest pusty");

            if (string.IsNullOrEmpty(user.Email) || string.IsNullOrEmpty(user.Password))
                return BadRequest("Email albo haslo sa puste");

            var userDb = await _context.Users.SingleOrDefaultAsync(x => x.Email == user.Email);

            // check if username exists
            if (userDb == null)
                return BadRequest("Nie ma takiego uzytkownika");

            // check if password is correct
            if (userDb.Password != user.Password)
                return BadRequest("Email albo haslo sie nie zgadzaja");

            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
            var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

            var tokeOptions = new JwtSecurityToken(
                //serwer wydający token
                issuer: "http://localhost:5000",
                //serwer odbierający token
                audience: "http://localhost:5000",
                //lista ról usera
                claims: new List<Claim>(),
                //data ekspiracji tokena
                expires: DateTime.Now.AddDays(7),
                signingCredentials: signinCredentials
            );

            var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
            return Ok(new 
            {
                Id = userDb.Id,
                Name = userDb.Name,
                Email = user.Email,
                Token = tokenString
            });

        }
    }
}