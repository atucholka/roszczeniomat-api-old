using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using roszczeniomat.Models;

namespace roszczeniomat.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("EnableCORS")]
    public class UserController : Controller
    {
        private readonly MainContext _context;

        public UserController(MainContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _context.Users.ToListAsync();

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpGet]
        [Route("{userId}")]
        public async Task<IActionResult> GetAsync(int userId)
        {
            var result = await _context.Users
                .SingleOrDefaultAsync(m => m.Id == userId);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }


        [HttpPut]
        public async Task<IActionResult> Update([FromBody]Users user)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userDb = await _context.Users.SingleOrDefaultAsync(m => m.Id == user.Id);

            if (userDb == null)
                return NotFound();

            userDb.Name = user.Name;
            userDb.Email = user.Email;
            // if (user.Password.Length > 0)
            // {
            //     userDb.Password = user.Password;
            // }
            userDb.Gender = user.Gender;
            _context.Users.Update(userDb);
            await _context.SaveChangesAsync();

            return Ok(userDb);
        }


        [HttpPost]
        public async Task<IActionResult> Create([FromBody]Users user)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return Ok(user);

        }


        // [HttpPut]
        // public async Task<IActionResult> UpdateUser([FromBody]Users user)
        // {
        //     if (!ModelState.IsValid)
        //         return BadRequest(ModelState);

        //     var userDb = _context.Users.FirstOrDefault(u => u.Id == user.Id);

        //     userDb.Name = user.Name;
        //     userDb.Email = user.Email;

        //     _context.Users.Update(userDb);
        //     await _context.SaveChangesAsync();
            
        //     return Ok();
        // }


        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var itemToDelete = new Users { Id = id };
            _context.Users.Attach(itemToDelete);
            _context.Entry(itemToDelete).State = EntityState.Deleted;
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}
