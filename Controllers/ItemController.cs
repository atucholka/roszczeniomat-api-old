using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using roszczeniomat.Models;
using roszczeniomat.Services;
using roszczeniomat.Interfaces;
using roszczeniomat.Entities;

namespace roszczeniomat.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("EnableCORS")]
    public class ItemController : Controller
    {
        private readonly MainContext _context;
        private readonly IItemService _itemService;
        public ItemController(MainContext context, IItemService itemService)
        {
            _context = context;
            _itemService = itemService;
        }

        [HttpPost]
        public async Task<IActionResult> AddItemAsync([FromBody]Items item)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _itemService.AddItemAsync(item);

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _itemService.ListAllItemsAsync();

            if (result == null)
                return NotFound();

            return Ok(result);
        }


        [HttpGet]
        [Route("filterbybeneficientsids")]
        public async Task<IActionResult> FilterByBeneficientsIds([FromQuery]int[] id)
        {
            var result = _itemService.FilterByBeneficientsIds(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpGet]
        [Route("filter")]
        public async Task<IActionResult> Filter([FromQuery] ItemsFilterEntity filter)
        {
            var result = _itemService.Filter(filter);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpGet]
        [Route("getbyid/{itemId}")]
        public async Task<IActionResult> GetAsyncById(int itemId)
        {
            var result = await _itemService.GetItemByIdAsync(itemId);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("bybenefactor/{userId}")]
        public async Task<IActionResult> GetAsyncByBenefactorId(int userId)
        {
            var result = _itemService.GetItemsByBenefactorId(userId);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("bybeneficient/{userId}")]
        public async Task<IActionResult> GetAsyncByBeneficientId(int userId)
        {
            var result = await _itemService.GetItemsByBeneficientIdAsync(userId);

            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [HttpPost]
        [Route("markAsBought")]
        public async Task<IActionResult> MarkAsBoughtAsync([FromBody]Items item)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _itemService.MarkAsBoughtAsync(item.Id);

            return Ok();
        }

        [HttpPost]
        [Route("markAsUnbought")]
        public async Task<IActionResult> MarkAsUnboughtAsync([FromBody]Items item)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _itemService.MarkAsUnboughtAsync(item.Id);

            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody]Items item)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _itemService.UpdateItemAsync(item);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _itemService.DeleteItemAsync(id);
            return Ok();
        }

    }
}
