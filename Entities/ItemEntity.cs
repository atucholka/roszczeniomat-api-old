﻿using System.Collections.Generic;

namespace roszczeniomat.Entities
{
    public class ItemEntity : BaseEntity
    {
        public int BeneficientId { get; set; }
        public string BeneficientName { get; set; }
        public bool Bought { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public int WantedScale { get; set; }
        public int ApproxItemPrice { get; set; }
        public List<ItemShareEntity> Shares { get; set; }
    }
}