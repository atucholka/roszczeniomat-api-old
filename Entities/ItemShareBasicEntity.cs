namespace roszczeniomat.Entities
{
    public class ItemShareBasicEntity : BaseEntity
    {
        public int ItemId { get; set; }
        public int BenefactorId { get; set; }
        public int Amount { get; set; }
        public int ApproxItemPrice { get; set; }
        public bool FullBook { get; set; }

    }
}