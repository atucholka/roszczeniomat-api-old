namespace roszczeniomat.Entities
{
    public class ItemShareEntity : BaseEntity
    {
        public int ItemId { get; set; }
        public int BenefactorId { get; set; }
        public string BenefactorName { get; set; }
        public string BenefactorGender { get; set; }
        public int Amount { get; set; }
        public bool FullBook { get; set; }
    }
}