using System.Collections.Generic;

namespace roszczeniomat.Entities
{
    public class ItemsFilterEntity
    {
        //wyszukiwanie po imieniu beneficjenta, benefaktorow, urlu i opisie
        public string Search { get; set; }
        public int WithoutBeneficientId { get; set; }
        public IEnumerable<int> BeneficientsIds { get; set; }
        public IEnumerable<int> BenefactorsIds { get; set; }
        public bool WithoutBooked { get; set; }
        public bool WithoutBought { get; set; }
    }
}