using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using roszczeniomat.Entities;

namespace roszczeniomat.Models
{
    public class Items : BaseEntity
    {
        public int BeneficientId { get; set; }
        public bool Bought { get; set; }
        [Required]
        public string Url { get; set; }
        public string Description { get; set; }
        public int WantedScale { get; set; }
        public int ApproxItemPrice { get; set; }
    }
}