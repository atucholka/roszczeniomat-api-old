using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using roszczeniomat.Entities;

namespace roszczeniomat.Models
{
    public class ItemShare : BaseEntity
    {
        [Required]
        public int ItemId { get; set; }
        [Required]
        public int BenefactorId { get; set; }
        [Required]
        public int Amount { get; set; }
        public bool FullBook { get; set; }
    }
}