using Microsoft.EntityFrameworkCore;
using roszczeniomat.Models;

namespace roszczeniomat.Models
{
    public class MainContext: DbContext
    {
        public MainContext(DbContextOptions<MainContext> options)
            : base(options)
        {
        }

        public DbSet<Items> Items { get; set; }
        public DbSet<ItemShare> ItemShare { get; set; }
        public DbSet<Users> Users { get; set; }
    }
}