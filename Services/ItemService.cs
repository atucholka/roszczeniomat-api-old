using System.Collections.Generic;
using System.Threading.Tasks;
using roszczeniomat.Entities;
using Microsoft.AspNetCore.Mvc;
using roszczeniomat.Interfaces;
using roszczeniomat.Models;

namespace roszczeniomat.Services
{
    public class ItemService : IItemService
    {
        private readonly IItemRepository _itemRepository;

        public ItemService(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public async Task AddItemAsync(Items item)
        {
            await _itemRepository.AddItemAsync(item);
        }

        public async Task<List<ItemEntity>> ListAllItemsAsync()
        {
            return await _itemRepository.ListAllItemsAsync();
        }

        public async Task MarkAsBoughtAsync(int itemId)
        {
            await _itemRepository.MarkAsBoughtAsync(itemId);
        }

        public async Task MarkAsUnboughtAsync(int itemId)
        {
            await _itemRepository.MarkAsUnboughtAsync(itemId);
        }

        public async Task<ItemEntity> GetItemByIdAsync(int itemId)
        {
            return await _itemRepository.GetItemByIdAsync(itemId);
        }
        public List<ItemEntity> GetItemsByBenefactorId(int benefactorId)
        {
            return _itemRepository.GetItemsByBenefactorId(benefactorId);
        }

        public List<ItemEntity> GetUnbookedAndUnbought()
        {
            var unBookedItems = _itemRepository.GetUnbooked();
            var unBoughtItems = _itemRepository.GetUnbought();
            unBookedItems.AddRange(unBoughtItems);

            return unBookedItems;
        }

        public List<ItemEntity> Filter(ItemsFilterEntity filter)
        {
            return _itemRepository.Filter(filter);
        }
        public List<ItemEntity> FilterByBeneficientsIds(IEnumerable<int> beneficientsIds)
        {
            return _itemRepository.FilterByBeneficientsIds(beneficientsIds);
        }

        public async Task<List<ItemEntity>> GetItemsByBeneficientIdAsync(int beneficientId)
        {
            return await _itemRepository.GetItemsByBeneficientIdAsync(beneficientId);
        }

        public async Task UpdateItemAsync(Items item)
        {
            await _itemRepository.UpdateItemAsync(item);
        }

        public async Task DeleteItemAsync(int itemId)
        {
            await _itemRepository.DeleteItemAsync(itemId);
        }
    }
}