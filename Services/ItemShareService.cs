using System.Linq;
using System.Threading.Tasks;
using roszczeniomat.Entities;
using roszczeniomat.Interfaces;
using roszczeniomat.Models;

namespace roszczeniomat.Services
{
    public class ItemShareService : IItemShareService
    {
        private readonly IItemShareRepository _itemShareRepository;
        private readonly IItemRepository _itemRepository;

        public ItemShareService(IItemRepository itemRepository, IItemShareRepository itemShareRepository)
        {
            _itemRepository = itemRepository;
            _itemShareRepository = itemShareRepository;
        }

        public async Task AddItemShareAsync(ItemShareBasicEntity itemShare)
        {
            var itemDb = _itemRepository.GetItemById(itemShare.ItemId);
            if (itemDb == null)
            {
                throw new System.Exception("Item not found test");
            }

            if (itemDb.Bought == true)
            {
                throw new System.ArgumentException("Item is already bought", "original");
            }

            // if (itemDb.ApproxItemPrice < itemDb.Shares.Sum(s => s.Amount) || itemDb.BeneficientId == itemShare.BenefactorId)
            // {
            //     throw new System.ArgumentException("Amount is incorrect", "original");
            // }

            // if (itemDb.BeneficientId == itemShare.BenefactorId)
            // {
            //     throw new System.ArgumentException("Benefactor can't create or participate in item share made for his item", "original");
            // }

            var itemShareData = new ItemShare();

            itemShareData.ItemId = itemShare.ItemId;
            itemShareData.BenefactorId = itemShare.BenefactorId;
            itemShareData.FullBook = itemShare.FullBook;
            itemShareData.Amount = itemShare.FullBook == true ? itemDb.ApproxItemPrice : itemShare.Amount;

            await _itemShareRepository.AddItemShareAsync(itemShareData);

            var itemToUpdate = new Items();

            itemToUpdate.Id = itemShare.ItemId;
            itemToUpdate.ApproxItemPrice = itemShare.ApproxItemPrice;

            await _itemRepository.UpdateItemPriceAsync(itemToUpdate);
        }

        public async Task DeleteItemShareAsync(int itemShareId)
        {
            await _itemShareRepository.DeleteItemShareAsync(itemShareId);
        }
    }
}