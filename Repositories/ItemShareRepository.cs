using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using roszczeniomat.Entities;
using roszczeniomat.Interfaces;
using roszczeniomat.Models;

namespace roszczeniomat.Repositories
{
    public class ItemShareRepository : EfRepository<ItemShare>, IItemShareRepository
    {
        public ItemShareRepository(MainContext dbContext) : base(dbContext)
        {
        }

        public Task AddItemShareAsync(ItemShare itemShare)
        {
            _dbContext.ItemShare.Add(itemShare);
            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteItemShareAsync(int itemShareId)
        {
            var itemToDelete = new ItemShare { Id = itemShareId };

            _dbContext.ItemShare.Attach(itemToDelete);
            _dbContext.Entry(itemToDelete).State = EntityState.Deleted;

            // _dbContext.ItemShare.Remove(itemToDelete);
            return _dbContext.SaveChangesAsync();
        }
    }
}