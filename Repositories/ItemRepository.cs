using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using roszczeniomat.Entities;
using roszczeniomat.Interfaces;
using roszczeniomat.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace roszczeniomat.Repositories
{
    public class ItemRepository : EfRepository<Items>, IItemRepository, IAsyncRepository<Items>
    {
        public ItemRepository(MainContext dbContext) : base(dbContext)
        {
        }

        public Task AddItemAsync(Items item)
        {
            item.ApproxItemPrice = 0;
            _dbContext.Items.Add(item);
            return _dbContext.SaveChangesAsync();
        }
        public List<ItemEntity> ListAllItems()
        {
            var query = (from i in _dbContext.Items
                         join u in _dbContext.Users on i.BeneficientId equals u.Id
                         select new ItemEntity
                         {
                             Id = i.Id,
                             BeneficientId = i.BeneficientId,
                             BeneficientName = u.Name,
                             Bought = i.Bought,
                             Url = i.Url,
                             Description = i.Description,
                             WantedScale = i.WantedScale,
                             ApproxItemPrice = i.ApproxItemPrice,
                             Shares = (from ish in _dbContext.ItemShare
                                       join user in _dbContext.Users on ish.BenefactorId equals user.Id
                                       where ish.ItemId == i.Id
                                       select new ItemShareEntity
                                       {
                                           Id = ish.Id,
                                           ItemId = ish.ItemId,
                                           BenefactorId = ish.BenefactorId,
                                           BenefactorName = user.Name,
                                           BenefactorGender = user.Gender,
                                           Amount = ish.Amount,
                                           FullBook = ish.FullBook
                                       }).ToList()
                         }).ToList();
            return query;
        }
        public Task<List<ItemEntity>> ListAllItemsAsync()
        {
            var query = (from i in _dbContext.Items
                         join u in _dbContext.Users on i.BeneficientId equals u.Id
                         select new ItemEntity
                         {
                             Id = i.Id,
                             BeneficientId = i.BeneficientId,
                             BeneficientName = u.Name,
                             Bought = i.Bought,
                             Url = i.Url,
                             Description = i.Description,
                             WantedScale = i.WantedScale,
                             ApproxItemPrice = i.ApproxItemPrice,
                             Shares = (from ish in _dbContext.ItemShare
                                       join user in _dbContext.Users on ish.BenefactorId equals user.Id
                                       where ish.ItemId == i.Id
                                       select new ItemShareEntity
                                       {
                                           Id = ish.Id,
                                           ItemId = ish.ItemId,
                                           BenefactorId = ish.BenefactorId,
                                           BenefactorName = user.Name,
                                           BenefactorGender = user.Gender,
                                           Amount = ish.Amount,
                                           FullBook = ish.FullBook
                                       }).ToList()
                         }).ToListAsync();
            return query;
        }
        public ItemEntity GetItemById(int itemId)
        {
            var itemShares = (from ish in _dbContext.ItemShare
                              join u in _dbContext.Users on ish.BenefactorId equals u.Id
                              where ish.ItemId == itemId
                              select new ItemShareEntity
                              {
                                  Id = ish.Id,
                                  ItemId = ish.ItemId,
                                  BenefactorId = ish.BenefactorId,
                                  BenefactorName = u.Name,
                                  BenefactorGender = u.Gender,
                                  Amount = ish.Amount,
                                  FullBook = ish.FullBook
                              }).ToList();

            var query = (from i in _dbContext.Items
                         join u in _dbContext.Users on i.BeneficientId equals u.Id
                         where i.Id == itemId
                         select new ItemEntity
                         {
                             BeneficientId = i.BeneficientId,
                             BeneficientName = u.Name,
                             Bought = i.Bought,
                             Url = i.Url,
                             Description = i.Description,
                             WantedScale = i.WantedScale,
                             ApproxItemPrice = i.ApproxItemPrice,
                             Shares = itemShares
                         }).FirstOrDefault();
            return query;
        }
        public Task<ItemEntity> GetItemByIdAsync(int itemId)
        {
            var itemShares = (from ish in _dbContext.ItemShare
                              join u in _dbContext.Users on ish.BenefactorId equals u.Id
                              where ish.ItemId == itemId
                              select new ItemShareEntity
                              {
                                  ItemId = ish.ItemId,
                                  BenefactorId = ish.BenefactorId,
                                  BenefactorName = u.Name,
                                  BenefactorGender = u.Gender,
                                  Amount = ish.Amount
                              }).ToList();

            var query = (from i in _dbContext.Items
                         join u in _dbContext.Users on i.BeneficientId equals u.Id
                         where i.Id == itemId
                         select new ItemEntity
                         {
                             Id = i.Id,
                             BeneficientId = i.BeneficientId,
                             BeneficientName = u.Name,
                             Bought = i.Bought,
                             Url = i.Url,
                             Description = i.Description,
                             WantedScale = i.WantedScale,
                             Shares = itemShares
                         }).FirstOrDefaultAsync();
            return query;
        }
        public List<ItemEntity> GetItemsByBenefactorId(int benefactorId)
        {
            var itemShares = (from ish in _dbContext.ItemShare
                              join u in _dbContext.Users on ish.BenefactorId equals u.Id
                              where ish.BenefactorId == benefactorId
                              select new ItemShareEntity
                              {
                                  ItemId = ish.ItemId,
                                  BenefactorId = ish.BenefactorId,
                                  BenefactorName = u.Name,
                                  BenefactorGender = u.Gender,
                                  Amount = ish.Amount
                              }).ToList();

            IEnumerable<int> itemsIds = itemShares.Select(ish => ish.ItemId);

            List<ItemEntity> items = new List<ItemEntity>();

            foreach (var itemId in itemsIds)
            {
                Items item = _dbContext.Items.FirstOrDefault(i => i.Id == itemId);
                ItemEntity itemEntity = GetItemById(itemId);
                items.Add(itemEntity);
            }
            return items;
        }
        public List<ItemEntity> GetItemsByBeneficientId(int beneficientId)
        {
            var query = (from i in _dbContext.Items
                         join u in _dbContext.Users on i.BeneficientId equals u.Id
                         where i.BeneficientId == beneficientId
                         select new ItemEntity
                         {
                             Id = i.Id,
                             BeneficientId = i.BeneficientId,
                             BeneficientName = u.Name,
                             Bought = i.Bought,
                             Url = i.Url,
                             Description = i.Description,
                             WantedScale = i.WantedScale,
                             ApproxItemPrice = i.ApproxItemPrice,
                             Shares = (from ish in _dbContext.ItemShare
                                       join u in _dbContext.Users on ish.BenefactorId equals u.Id
                                       where ish.ItemId == i.Id
                                       select new ItemShareEntity
                                       {
                                           Id = ish.Id,
                                           ItemId = ish.ItemId,
                                           BenefactorId = ish.BenefactorId,
                                           BenefactorName = u.Name,
                                           BenefactorGender = u.Gender,
                                           Amount = ish.Amount,
                                           FullBook = ish.FullBook
                                       }).ToList()
                         }).ToList();

            return query;
        }
        public Task<List<ItemEntity>> GetItemsByBeneficientIdAsync(int beneficientId)
        {
            var query = (from i in _dbContext.Items
                         join u in _dbContext.Users on i.BeneficientId equals u.Id
                         where i.BeneficientId == beneficientId
                         select new ItemEntity
                         {
                             Id = i.Id,
                             BeneficientId = i.BeneficientId,
                             BeneficientName = u.Name,
                             Bought = i.Bought,
                             Url = i.Url,
                             Description = i.Description,
                             WantedScale = i.WantedScale,
                             ApproxItemPrice = i.ApproxItemPrice,
                             Shares = (from ish in _dbContext.ItemShare
                                       join u in _dbContext.Users on ish.BenefactorId equals u.Id
                                       where ish.ItemId == i.Id
                                       select new ItemShareEntity
                                       {
                                           Id = ish.Id,
                                           ItemId = ish.ItemId,
                                           BenefactorId = ish.BenefactorId,
                                           BenefactorName = u.Name,
                                           BenefactorGender = u.Gender,
                                           Amount = ish.Amount,
                                           FullBook = ish.FullBook
                                       }).ToList()
                         }).ToListAsync();

            return query;
        }

        public List<ItemEntity> Filter(ItemsFilterEntity filter)
        {
            // string search = filter.Search;
            IEnumerable<int> beneficientsIds = filter.BeneficientsIds;
            // IEnumerable<int> benefactorsIds = filter.BenefactorsIds;
            int beneficientToExclude = filter.WithoutBeneficientId;
            bool withoutBooked = filter.WithoutBooked;
            bool withoutBought = filter.WithoutBought;

            List<ItemEntity> items = FilterByBeneficientsIds(beneficientsIds);
            items.AddRange(ExcludeBeneficientItems(beneficientToExclude));
            items.AddRange(GetUnbooked());
            items.AddRange(GetUnbought());

            var total = items.Distinct().ToList();

            return total;
        }
        public List<ItemEntity> FilterByBeneficientsIds(IEnumerable<int> beneficientsIds)
        {
            List<ItemEntity> items = new List<ItemEntity>();

            foreach (var beneficientId in beneficientsIds)
            {
                List<ItemEntity> beneficientItems = GetItemsByBeneficientId(beneficientId);
                items.AddRange(beneficientItems);
            }

            return items;
        }

        public List<ItemEntity> ExcludeBeneficientItems(int beneficientId)
        {
            var itemsIds = (from i in _dbContext.Items
                            where i.BeneficientId != beneficientId
                            select i).ToList();
            List<ItemEntity> items = new List<ItemEntity>();

            foreach (var i in itemsIds)
            {
                List<ItemEntity> beneficientItems = GetItemsByBeneficientId(i.BeneficientId);
                items.AddRange(beneficientItems);
            }

            return items;
        }
        public List<ItemEntity> GetUnbooked()
        {
            List<ItemShare> allShares = (from ish in _dbContext.ItemShare
                                         where ish.FullBook == false
                                         select ish).ToList();
            List<ItemEntity> items = new List<ItemEntity>();

            foreach (var itemShare in allShares)
            {
                var item = GetItemById(itemShare.ItemId);
                items.Add(item);
            }

            return items;
        }
        public List<ItemEntity> GetUnbought()
        {
            var query = (from i in _dbContext.Items
                         join u in _dbContext.Users on i.BeneficientId equals u.Id
                         where i.Bought == false
                         select new ItemEntity
                         {
                             Id = i.Id,
                             BeneficientId = i.BeneficientId,
                             BeneficientName = u.Name,
                             Bought = i.Bought,
                             Url = i.Url,
                             Description = i.Description,
                             WantedScale = i.WantedScale,
                             ApproxItemPrice = i.ApproxItemPrice,
                             Shares = (from ish in _dbContext.ItemShare
                                       join user in _dbContext.Users on ish.BenefactorId equals user.Id
                                       where ish.ItemId == i.Id
                                       select new ItemShareEntity
                                       {
                                           Id = ish.Id,
                                           ItemId = ish.ItemId,
                                           BenefactorId = ish.BenefactorId,
                                           BenefactorName = user.Name,
                                           BenefactorGender = user.Gender,
                                           Amount = ish.Amount,
                                           FullBook = ish.FullBook
                                       }).ToList()
                         }).ToList();

            return query;
        }
        public Task MarkAsBoughtAsync(int itemId)
        {
            var itemDb = _dbContext.Items.FirstOrDefault(i => i.Id == itemId);
            itemDb.Bought = true;

            _dbContext.Items.Update(itemDb);

            return _dbContext.SaveChangesAsync();
        }

        public Task MarkAsUnboughtAsync(int itemId)
        {
            var itemDb = _dbContext.Items.FirstOrDefault(i => i.Id == itemId);
            itemDb.Bought = false;

            _dbContext.Items.Update(itemDb);

            return _dbContext.SaveChangesAsync();
        }

        public Task UpdateItemAsync(Items item)
        {
            var itemDb = _dbContext.Items.FirstOrDefault(i => i.Id == item.Id);

            itemDb.Url = item.Url;
            itemDb.Description = item.Description;
            itemDb.WantedScale = item.WantedScale;
            itemDb.ApproxItemPrice = item.ApproxItemPrice;

            _dbContext.Items.Update(itemDb);

            return _dbContext.SaveChangesAsync();
        }

        public Task UpdateItemPriceAsync(Items item)
        {
            var itemDb = _dbContext.Items.FirstOrDefault(i => i.Id == item.Id);

            itemDb.ApproxItemPrice = item.ApproxItemPrice;

            _dbContext.Items.Update(itemDb);

            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteItemAsync(int itemId)
        {
            var itemToDelete = new Items { Id = itemId };

            _dbContext.Items.Attach(itemToDelete);
            _dbContext.Entry(itemToDelete).State = EntityState.Deleted;

            return _dbContext.SaveChangesAsync();
        }
    }
}