﻿using System.Collections.Generic;
using System.Threading.Tasks;
using roszczeniomat.Entities;

namespace roszczeniomat.Interfaces
{
    public interface IAsyncRepository<T> where T : BaseEntity
    {
        Task<T> AddAsync(T entity);
        // Task<T> GetByIdAsync(int id);
        // Task<List<T>> GetAllAsync();
        // Task UpdateAsync(T entity);
        // Task DeleteAsync(T entity);
    }
}
