using System.Threading.Tasks;
using roszczeniomat.Entities;
using roszczeniomat.Models;

namespace roszczeniomat.Interfaces
{
    public interface IItemShareRepository
    {
        Task AddItemShareAsync(ItemShare itemShare);
        Task DeleteItemShareAsync(int itemShareId);
    }
}