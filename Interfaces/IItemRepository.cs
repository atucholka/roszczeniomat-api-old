using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using roszczeniomat.Entities;
using roszczeniomat.Models;

namespace roszczeniomat.Interfaces
{
    public interface IItemRepository
    {
        Task AddItemAsync(Items item);
        Task<List<ItemEntity>> ListAllItemsAsync();
        ItemEntity GetItemById(int itemId);
        Task<ItemEntity> GetItemByIdAsync(int itemId);
        List<ItemEntity> GetItemsByBenefactorId(int benefactorId);
        List<ItemEntity> GetItemsByBeneficientId(int beneficientId);
        Task<List<ItemEntity>> GetItemsByBeneficientIdAsync(int beneficientId);
        List<ItemEntity> GetUnbooked();
        List<ItemEntity> GetUnbought();
        List<ItemEntity> Filter(ItemsFilterEntity filter);
        List<ItemEntity> ExcludeBeneficientItems(int beneficientId);
        List<ItemEntity> FilterByBeneficientsIds(IEnumerable<int> beneficientsIds);
        Task MarkAsBoughtAsync(int itemId);
        Task MarkAsUnboughtAsync(int itemId);
        Task UpdateItemAsync(Items item);
        Task UpdateItemPriceAsync(Items item);
        Task DeleteItemAsync(int itemId);
    }
}