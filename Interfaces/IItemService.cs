using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using roszczeniomat.Entities;
using roszczeniomat.Models;

namespace roszczeniomat.Interfaces
{
    public interface IItemService
    {
        Task AddItemAsync(Items item);
        Task<List<ItemEntity>> ListAllItemsAsync();
        Task<ItemEntity> GetItemByIdAsync(int itemId);
        List<ItemEntity> GetItemsByBenefactorId(int benefactorId);
        Task<List<ItemEntity>> GetItemsByBeneficientIdAsync(int beneficientId);
        List<ItemEntity> GetUnbookedAndUnbought();
        List<ItemEntity> Filter(ItemsFilterEntity filter);
        List<ItemEntity> FilterByBeneficientsIds(IEnumerable<int> beneficientsIds);
        Task MarkAsBoughtAsync(int itemId);
        Task MarkAsUnboughtAsync(int itemId);
        Task UpdateItemAsync(Items item);
        Task DeleteItemAsync(int itemId);
    }
}