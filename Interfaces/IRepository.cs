using System.Collections.Generic;
using roszczeniomat.Entities;

namespace roszczeniomat.Interfaces
{
    public interface IRepository<T> where T : BaseEntity
    {
        T GetById(int id);
        // IEnumerable<T> ListAll();
        T Add(T entity);
        void Update(T entity);
        // void Delete(int id);
    }
}
