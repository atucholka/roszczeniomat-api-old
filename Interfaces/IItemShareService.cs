using System.Threading.Tasks;
using roszczeniomat.Entities;
using roszczeniomat.Models;

namespace roszczeniomat.Interfaces
{
    public interface IItemShareService
    {
        Task AddItemShareAsync(ItemShareBasicEntity itemShare);
        Task DeleteItemShareAsync(int itemShareId);
    }
}